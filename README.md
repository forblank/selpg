# 使用 Golang 开发 selpg

## 目录

- [概述](#0)
- [获取和安装](#1)
- [使用说明](#2)
- [设计说明](#3)
    - [C源代码分析](#3.1)
    - [Golang实现](#3.2)
        - [导入包](#3.2.1)
        - [结构体 selpgArgs](#3.2.2)
        - [函数 ProcessArgs](#3.2.3)
        - [函数 ProcessInput](#3.2.4)
        - [函数 Usage](#3.2.5)
        - [函数 main](#3.2.6)
    - [要点说明](#3.3)
- [测试结果](#4)
    - [单元测试 & 示例测试](#4.1)
        - [TestProcessArgs](#4.1.1)
        - [ExampleProcessArgs](#4.1.2)
        - [TestProcessInput](#4.1.3)
        - [ExampleProcessInput](#4.1.4)
        - [go test 结果](#4.1.5)
    - [功能测试](#4.2)
- [总结](#5)
- [参考资料](#6)



<a name="0"/>

## 概述

selpg 是从文本输入选择页范围的实用程序。该输入可以来自作为最后一个命令行参数指定的文件，在没有给出文件名参数时也可以来自标准输入。selpg 首先处理所有的命令行参数。在扫描了所有的选项参数（也就是那些以连字符为前缀的参数）后，如果 selpg 发现还有一个参数，则它会接受该参数为输入文件的名称并尝试打开它以进行读取。如果没有其它参数，则 selpg 假定输入来自标准输入。

<a name="1"/>

## 获取和安装
```
go get gitee.com/forblank/selpg

go install gitee.com/forblank/selpg

```


<a name="2"/>

## 使用说明

```
selpg -sstartPage -eendPage [ -f | -lpageLen ] [ -dprintDest ] [ inFilename ]
```

必选参数

- `-sstartPage` ：指明从 `startPage` 页开始读取
- `-eendPage` ：指明从 `endPage` 页结束读取

可选参数

- `-lpageLen` ：指明 `pageLen` 为每一页的行数，默认为72
- `-f` ：指明在输入中寻找换页符，并将其作为页定界符处理
- `-dprintDest` ：输出文件路径，默认为标准输出
- `inFilename` ：输入文件路径，默认为标准输入

文件路径要求

只需要输入文件相对于终端所处位置的相对路径

```
root/
  ├ example1/ -> if open cmd here, use " selpg ... -dout/outFile.txt in/inFile.txt "
  |  ├ in/
  |  |  └ inFile.txt
  |  └ out/
  |     └ outFile.txt
  |
  └ example2/ -> if open cmd here, use " selpg ... -doutFile.txt inFile.txt "
     ├ inFile.txt
     └ outFile.txt
```



<a name="3"/>

## 设计说明

<a name="3.1"/>

### C源代码分析

结构体 `selpg_args` ：存储参数

函数 `process_args` ：解析输入参数，存入参数结构体中

函数 `process_input` ：根据参数设定输入输出源，将从输入源读取指定范围的页到输出源

函数 `usage` ：打印使用信息

函数 `main` ：调用 `process_args` 解析参数，调用 `process_input` 进行读取

<a name="3.2"/>

### Golang实现

参考 C 源代码，结合使用 `fmt` 、`io` 、`os` 、`bufio` 、`os/exec` 、`pflag` 包，进行 Golang 的实现

<a name="3.2.1"/>

#### 导入包

```go
import (
    "fmt"
	"io"
    "os"
    "bufio"
	"os/exec"
 	pflag "github.com/spf13/pflag"
)
```

<a name="3.2.2"/>

#### 结构体 selpgArg

```go
type selpgArgs struct {
	startPage int
	endPage int
	inFilename string
	pageLen int
	pageType bool
	printDest string
}
```

<a name="3.2.3"/>

#### 函数 ProcessArgs

```go
// ProcessArgs 解析输入参数，存入参数结构体中
func ProcessArgs(args *selpgArgs) {

	/* check the command-line arguments for validity */

	/* Not enough args, minimum command is "selpg -sstart_page -eend_page"  */
	if len(os.Args) < 3 {
		fmt.Fprintf(os.Stderr, "%s: not enough arguments\n", progname)
		Usage()
		os.Exit(1)
	}

	/* handle mandatory args first */
	var tmp string

	/* handle 1st arg - start page */
	tmp = os.Args[1]
	if tmp[0:2] != "-s" {
		fmt.Fprintf(os.Stderr, "%s: 1st arg should be -sstart_page\n", progname)
		Usage()
		os.Exit(2)
	}

	/* handle 2nd arg - end page */
	tmp = os.Args[2]
	if tmp[0:2] != "-e" {
		fmt.Fprintf(os.Stderr, "%s: 2nd arg should be -eend_page\n", progname)
		Usage()
		os.Exit(3)
	}

	/* Use pflag to bind each parameter */
	pflag.IntVarP(&args.startPage, "start", "s", -1, "Start Page.")
	pflag.IntVarP(&args.endPage, "end", "e", -1, "End Page.")
	pflag.IntVarP(&args.pageLen, "len", "l", 10, "Lines per Page.")
	pflag.BoolVarP(&args.pageType, "type", "f", false, "Form-feed-delimited")
	pflag.StringVarP(&args.printDest, "destination", "d", "", "Specific Printer")
	pflag.Parse()

	/* parse the final parameter, the input file */
	if pflag.NArg() > 0 {
		args.inFilename = string(pflag.Arg(0))
	} else {
		args.inFilename = ""
	}

	/* Check the value range of each parameter */
	if args.pageLen < 1 {
		fmt.Fprintf(os.Stderr, "%s: invalid page length %v\n", progname, args.pageLen)
		os.Exit(4)
		Usage()
	}
	if args.startPage < 1 {
		fmt.Fprintf(os.Stderr, "%s: invalid start page %v\n", progname, args.startPage)
		os.Exit(5)
		Usage()
	}
	if (args.startPage > args.endPage) || (args.endPage < 1) {
		fmt.Fprintf(os.Stderr, "%s: invalid end page %v\n", progname, args.endPage)
		os.Exit(6)
		Usage()
	}
}
```

<a name="3.2.4"/>

#### 函数 ProcessInput

```go
func ProcessInput(args *selpgArgs) {

	var inFile *os.File
	var outFile io.WriteCloser
	var cmd *exec.Cmd
	pwd, _ := os.Getwd()

	/* set the input source */
	if args.inFilename == "" {
		inFile = os.Stdin
	} else {
		var inFileError error
		inFile, inFileError = os.Open(pwd + "/" + args.inFilename)
		if inFileError != nil {
			fmt.Fprintf(os.Stderr, "%s: could not open input file \"%s\"\n", progname, args.inFilename)
			os.Exit(7)
		}
	}

	/* set the output destination */
	if args.printDest == "" {
		outFile = os.Stdout
	} else {
		/* Use "cat" as a printer */
		cmd = exec.Command("cat")
		var outFileError error
		cmd.Stdout, outFileError = os.OpenFile(pwd+"/"+args.printDest, os.O_WRONLY|os.O_TRUNC, 0600)
		if outFileError != nil {
			fmt.Fprintf(os.Stderr, "%s: file not exist\n%s\n", progname, args.printDest)
			os.Exit(8)
		}
		outFile, _ = cmd.StdinPipe()
		cmd.Start()
	}

	/* begin one of two main loops based on page type */

	pageCount := 1
	buffer := bufio.NewReader(inFile)

	if args.pageType {

		for {
			line, err := buffer.ReadString('\f')

			if err != io.EOF && err != nil {
				fmt.Fprintf(os.Stderr, "%s: could not read file\n%s\n", progname, args.inFilename)
				os.Exit(9)
			}

			pageCount++

			if (pageCount >= args.startPage) && (pageCount <= args.endPage) {
				_, err := outFile.Write([]byte(line))

				if err != nil {
					fmt.Fprintf(os.Stderr, "%s: could not read file\n%v\n", progname, err)
					os.Exit(10)
				}
			}

			if err == io.EOF {
				break
			}
		}
		return
	}

	lineCount := 0

	for {

		line, err := buffer.ReadString('\n')

		if err != io.EOF && err != nil {
			fmt.Fprintf(os.Stderr, "%s: could not read file\n%s\n", progname, args.inFilename)
			os.Exit(9)
		}

		lineCount++

		if lineCount > args.pageLen {
			pageCount++
			lineCount = 1
		}

		if (pageCount >= args.startPage) && (pageCount <= args.endPage) {
			_, err := outFile.Write([]byte(line))

			if err != nil {
				fmt.Fprintf(os.Stderr, "%s: could not read file\n%v\n", progname, err)
				os.Exit(10)
			}
		}

		if err == io.EOF {
			break
		}
	}

}
```

<a name="3.2.5"/>

#### 函数 Usage

```go
//Usage 打印使用信息
func Usage() {

	fmt.Fprintf(os.Stderr, "\nUSAGE: %s -sstartPage -eendPage [ -f | -lpageLen ] [ -dprintDest ] [ inFilename ]\n", progname)

}
```

<a name="3.2.6"/>

#### 函数 `main`

```go
func main() {
	progname = os.Args[0]
	var args selpgArgs
	ProcessArgs(&args)
	ProcessInput(&args)
}
```

<a name="3.3"/>

### 要点说明

- 使用 `pflag` 替代 `goflag` 以满足 Unix 命令行规范

    `pflag` 包相比单纯的通过 `os.Args` 切片分析命令行参数，提供了更强的能力；使用 `pflag.XxxVarP` 绑定参数到相应变量，使用 `pflag.Parse` 解析 ，使用 `pflag.NArg()` 获得剩下的未解析参数个数，如果有，取 `pflag.Arg(0)` 作为输入源

    ```go
    /* Use pflag to bind each parameter */
    pflag.IntVarP(&args.startPage, "start", "s", -1, "Start Page.")
    pflag.IntVarP(&args.endPage, "end", "e", -1, "End Page.")
    pflag.IntVarP(&args.pageLen, "len", "l", 10, "Lines per Page.")
    pflag.BoolVarP(&args.pageType, "type", "f", false, "Form-feed-delimited")
    pflag.StringVarP(&args.printDest, "destination", "d", "", "Specific Printer")
    pflag.Parse()
    
    /* parse the final parameter, the input file */
    if pflag.NArg() > 0 {
    	args.inFilename = string(pflag.Arg(0))
    } else {
    	args.inFilename = ""
    }
    ```

-  使用 `os` 包进行文件读写、读环境变量

    `os.Args` 获得命令行参数；`os.Stdin` 标准输入流、 `os.Stdout` 标准输出流、 `os.Stderr` 标准错误流；`os.Getwd` 获取当前路径； `os.Open` 打开文件；`os.Exit` 退出

    ```go
    pwd, _ := os.Getwd()
    //...
    inFile, inFileError = os.Open(pwd + "/" + args.inFilename)
    ```

    利用 `os.Getwd` 获取当前路径，避免写死路径找不到文件，使用时也无需输入过长的文件路径；

-  `-dXXX` 实现

    使用 `os/exec` 包，创建子进程执行外部命令，将输出的数据作为外部命令的输入

    - 使用 `exec.Command` 设定要执行的外部命令
    - 设置 `cmd.Stdout` 为输出源 `printDest`
    - `cmd.StdinPipe()` 返回连接到command标准输入的管道pipe ，设置其为进程输出 `outFile`
    - `cmd.Start()` 开始非阻塞执行命令

    ```go
    /* Use "cat" as a printer */
    cmd = exec.Command("cat")
    var outFileError error
    cmd.Stdout, outFileError = os.OpenFile(pwd + "/" + args.printDest, os.O_WRONLY|os.O_TRUNC, 0600)
    //...
    outFile, _ = cmd.StdinPipe()
    cmd.Start()
    ```

    由于没有打印机，使用 `cat` 命令替换 `lp` 命令，从而实现输出到文件中，不过目标文件需要先创建

<a name="4"/>

## 测试结果

由于C源代码中给出的默认每页包含行数为72，为了方便测试默认跨页读取，现调整为默认值10

终端位于输入输出文件所在目录

input.txt

```
Line1
Line2
...
Lin40

```

<a name="4.1"/>

### 单元测试 & 示例测试

<a name="4.1.1"/>

#### TestProcessArgs

```go
func TestProcessArgs(t *testing.T) {

	cases := []struct {
		cmd          []string
		expectedArgs selpgArgs
	}{
		{
			[]string{"selpg", "-s1", "-e1"},
			selpgArgs{1, 1, "", 10, false, ""},
		},
		{
			[]string{"selpg", "-s1", "-e1", "input.txt"},
			selpgArgs{1, 1, "input.txt", 10, false, ""},
		},
		{
			[]string{"selpg", "-s2", "-e2", "-doutput.txt"},
			selpgArgs{2, 2, "", 10, false, "output.txt"},
		},
		{
			[]string{"selpg", "-s1", "-e2", "-l15"},
			selpgArgs{1, 2, "", 15, false, ""},
		},
		{
			[]string{"selpg", "-s1", "-e2", "-f"},
			selpgArgs{1, 2, "", 10, true, ""},
		},
		{
			[]string{"selpg", "-s1", "-e2", "-l15", "-doutput.txt", "input.txt"},
			selpgArgs{1, 2, "input.txt", 15, false, "output.txt"},
		},
	}

	for i, c := range cases {
		var args selpgArgs
		os.Args = c.cmd
		pflag.CommandLine = pflag.NewFlagSet(os.Args[0], pflag.ExitOnError)
		ProcessArgs(&args)
		if args != c.expectedArgs {
			t.Errorf("case %v: got %v, expected %v", i, args, c.expectedArgs)
		}
	}

}
```

为了传递命令行参数给 `ProcessArgs` ，将 `os.Args` 指向测例中的 `cmd` ，`cmd` 是一个 string 数组，包含了 `ProcessArgs` 需要的命令行参数；

为了能使用 `pflag` 解析参数，需要将测例的命令行参数加入到 ` pflag.CommandLine` 中，这样调用 `pflag.Parse()` 时才能解析正确的命令行参数；

<a name="4.1.2"/>

#### ExampleProcessArgs

```go
func ExampleProcessArgs() {
	var args selpgArgs
	os.Args = []string{"selpg", "-s1", "-e2", "-l15", "-doutput.txt", "input.txt"}
	pflag.CommandLine = pflag.NewFlagSet(os.Args[0], pflag.ExitOnError)
	ProcessArgs(&args)
	fmt.Println(args)
	// Output: {1 2 input.txt 15 false output.txt}

}
```

检查命令行参数解析是否正确；提供示例；

<a name="4.1.3"/>

#### TestProcessInput

```go
func TestProcessInput(t *testing.T) {

	cases := []selpgArgs{
		selpgArgs{1, 1, "input.txt", 10, false, "output.txt"},
		selpgArgs{2, 2, "input.txt", 15, false, "output.txt"},
		selpgArgs{1, 2, "input.txt", 10, false, "output.txt"},
		selpgArgs{2, 3, "input.txt", 5, false, "output.txt"},
	}

	for i, args := range cases {
		ProcessInput(&args)
		time.Sleep(20 * time.Millisecond)
		pwd, _ := os.Getwd()
		outFile, _ := os.Open(pwd + "/" + args.printDest)
		lineCout := 0
		buffer := bufio.NewReader(outFile)

		for {
			line, err := buffer.ReadString('\n')
			if err == io.EOF {
				break
			}
			lineCout++
			line = strings.Replace(line, "\n", "", -1)
			expectedLine := "Line" + fmt.Sprint((args.startPage-1)*args.pageLen+lineCout)
			if line != expectedLine {
				t.Errorf("case %v: got %v, expected %v", i, line, expectedLine)
			}
		}
	}
}
```

适用于测试命令行参数含有 `-lpageLen` 和 `-dprintDest` 的情况，将读取 `printDest` 文件的内容，与预期内容相比较；

使用 `time.Sleep` 来保证测例中调用的 `ProcessInput` 的 `cat` 子进程运行结束，不然可能会出现 `cat` 子进程还未将内容写入 `printDest` 输出文件，就去读取 `printDest` 文件的内容，导致与预期内容不符合而报错；可以根据需要读取的内容长度调整睡眠时长，此处为 `20 * time.Millisecond`

<a name="4.1.4"/>

#### ExampleProcessInput

```go
func ExampleProcessInput() {
	args := selpgArgs{1, 1, "input.txt", 10, false, ""}
	ProcessInput(&args)
	//Output: Line1
	// Line2
	// Line3
	// Line4
	// Line5
	// Line6
	// Line7
	// Line8
	// Line9
	// Line10
}
```

适用于测试输出源为标准输出（屏幕）的情况，检查输出内容是否与预期内容相符合；提供示例；

<a name="4.1.5"/>

#### go test 结果



<img src="image/image-20201011134613283.png" alt="image-20201011134613283" style="zoom:80%;" />



<a name="4.2"/>

### 功能测试

1. 将 input.txt 的第 1 页写至标准输出（屏幕），因为这里没有重定向或管道； 

    ```
    selpg -s1 -e1 input.txt
    ```

    <img src="image/image-20201011134707409.png" alt="image-20201011134707409" style="zoom:80%;" />

    如图所示，默认每页包含10行，屏幕显示 “Line1” 到 “Line10”

    

2. selpg 读取标准输入，而标准输入已被 shell／内核重定向为来自 input_file.txt 而不是显式命名的文件名参数；输入的第 1 页被写至屏幕 ；

    ```
    selpg -s1 -e1 <input.txt
    ```

    <img src="image/image-20201011134802130.png" alt="image-20201011134802130" style="zoom:80%;" />

    如图所示，默认每页包含10行，屏幕显示 “Line1” 到 “Line10”

    

3. `ls` 命令的标准输出被 shell／内核重定向至 selpg 的标准输入；将第 1 页写至 selpg 的标准输出（屏幕）；

    ```
    ls | selpg -s1 -e1
    ```

    <img src="image/image-20201011134834680.png" alt="image-20201011134834680" style="zoom:80%;" />

    如图所示，屏幕显示当前所在目录的文件

    

4. selpg 将第 2 页到第 3 页写至标准输出；标准输出被 shell／内核重定向至 output.txt

    ```
    selpg -s2 -e3 input.txt >output.txt
    ```

    <img src="image/image-20201011135015789.png" alt="image-20201011135015789" style="zoom:80%;" />

    如图所示，默认每页包含10行，output.txt 包含 “Line11” 到 “Line30”

    

5. selpg 将第 2 页到第 3 页写至标准输出（屏幕）；所有的错误消息被 shell／内核重定向至 error.txt

    ```
    selpg -s2 -e3 input.txt 2>error.txt
    ```

    <img src="image/image-20201011135100634.png" alt="image-20201011135100634" style="zoom:80%;" />

    如图所示，默认每页包含10行，屏幕显示 “Line11” 到 “Line30”

    ```
    selpg -s2 -e0 input.txt 2>error.txt
    ```

    <img src="image/image-20201011135150887.png" alt="image-20201011135150887" style="zoom:80%;" />

    如图所示，error.txt 包含错误消息 “selpg: invalid end page 0”

    

6. selpg 将第 2 页到第 3 页写至标准输出，标准输出被重定向至 output.txt；selpg 写至标准错误的所有内容都被重定向至 error.txt

    ```
    selpg -s2 -e3 input.txt >output.txt 2>error.txt
    ```

    <img src="image/image-20201011135356114.png" alt="image-20201011135356114" style="zoom:80%;" />

    如图所示，默认每页包含10行，output.txt 包含 “Line11” 到 “Line30”

    

7. selpg 将第 2 页到第 3 页写至标准输出，标准输出被重定向至 output.txt；selpg 写至标准错误的所有内容都被重定向至 /dev/null（空设备），这意味着错误消息被丢弃了

    ```
    selpg -s2 -e3 input.txt >output.txt 2>/dev/null
    ```

    <img src="image/image-20201011135521983.png" alt="image-20201011135521983" style="zoom:80%;" />

    如图所示，默认每页包含10行，output.txt 包含从 “Line11” 到 “Line30”

    ```
    selpg -s0 -e3 input.txt >output.txt 2>/dev/null
    ```

    <img src="image/image-20201011135619269.png" alt="image-20201011135619269" style="zoom:80%;" />

    如图所示，output.txt无内容，error.txt无内容，错误消息被丢弃

    

8. selpg 将第 2 页到第 3 页写至标准输出，标准输出被丢弃；错误消息在屏幕出现

    ```
    selpg -s2 -e3 input.txt >/dev/null
    ```

    <img src="image/image-20201011135742810.png" alt="image-20201011135742810" style="zoom:80%;" />

    如图所示，屏幕无显示

    ```
    selpg -s2 -e0 input.txt >/dev/null
    ```

    <img src="image/image-20201011135822428.png" alt="image-20201011135822428" style="zoom:80%;" />

    如图所示，屏幕出现错误消息 “selpg: invalid end page 0”

    

9. selpg 的标准输出透明地被 shell／内核重定向，成为 `wd` 命令的标准输入，第 2 页到第 3 页被写至该标准输入； `wc` 命令会显示选定范围的页中包含的行数、字数和字符数；

    ```
    selpg -s2 -e3 input.txt | wc
    ```

    <img src="image/image-20201011140036684.png" alt="image-20201011140036684" style="zoom:80%;" />

    如图所示，屏幕显示 “20 20 140”；默认每页包含10行，输出页2到页2，有20行；每行的 “LineX” 记为一个字，有20个字；“LineX” 和 “\n” 记为7个字符，有140个字符

    

10. 与测试 9 相似，只有一点不同：错误消息被写至 error_file.txt

    ```
    selpg -s0 -e3 input.txt 2>error.txt | wc
    ```

    <img src="image/image-20201011140636421.png" alt="image-20201011140636421" style="zoom:80%;" />

    如图所示，屏幕显示 “0 0 0”，error.txt 包含错误消息 “selpg: invalid start page 0”

    

11. 将页长设置为 15 行，这样 selpg 就可以把输入当作被定界为该长度的页那样处理；第 2 页到第 3 页被写至 selpg 的标准输出（屏幕） 

      ```
     selpg -s2 -e3 -l15 input.txt
      ```

     <img src="image/image-20201011140902100.png" alt="image-20201011140902100" style="zoom:80%;" />

      如图所示，每页包含15行，屏幕显示 “Line16” 到 “Line40”

     

12. 假定页由换页符定界，由于 input.txt 中没有换页符，所以全部内容写至 selpg 的标准输出（屏幕）

      ```
     selpg -s1 -e2 -f input.txt
      ```

     <img src="image/image-20201011142921706.png" alt="image-20201011142921706" style="zoom:80%;" />

     如图所示，屏幕显示 “Line1” 到 “Line40”

13. 第 1 页到第 2 页由管道输送至 `cat` 命令，该命令将使输出写至 output.txt

     ```
     selpg -s1 -e2 -doutput.txt input.txt
     ```

     <img src="image/image-20201011143016507.png" alt="image-20201011143016507" style="zoom:80%;" />

     如图所示，默认每页包含10行，output.txt 包含从 “Line1” 到 “Line20”

     

14.  在“后台”运行进程，显示“进程标识”（pid），然后 shell 提示符几乎立刻会出现，能够继续向 shell 输入更多命令， selpg 进程在后台运行，并且标准输出和标准错误都被重定向至文件

     ```
     selpg -s1 -e2 input.txt > output.txt 2>error.txt &
     ```
     
     <img src="image/image-20201011143146478.png" alt="image-20201011143146478" style="zoom:80%;" />
     
     如图所示，默认每页包含10行，output.txt 包含从 “Line1” 到 “Line20”；由于太快执行完，未能输入更多命令



<a name="5"/>

## 总结

在这次实验中， 使用 golang 开发 selpg，结合使用 `fmt` 、`io` 、`os` 、`bufio` 、`os/exec` 、`pflag` 包，将 C 源代码“翻译”成 Golang 源代码，并使用 go test 进行测试；通过这次实验，学会了如何在 Golang 中读取解析命令行参数，对文件进行读写，对管道的使用，利用子进程执行命令，在编写测试的过程中，对 `os` 和 `flag` 包有了更深入的理解，探索出在测试中传递命令行参数并使其能够解析的办法；



<a name="6"/>

## 参考资料

[开发 Linux 命令行实用程序](https://www.ibm.com/developerworks/cn/linux/shell/clutil/index.html)

[标准库—命令行参数解析FLAG](http://blog.studygolang.com/2013/02/%E6%A0%87%E5%87%86%E5%BA%93-%E5%91%BD%E4%BB%A4%E8%A1%8C%E5%8F%82%E6%95%B0%E8%A7%A3%E6%9E%90flag/)

[Golang : flag 包简介](https://www.cnblogs.com/sparkdev/p/10812422.html)

[CLI: Command Line Programming with Go](https://thenewstack.io/cli-command-line-programming-with-go/)

[exec.Command](https://godoc.org/os/exec#Command)

[io.Pipe](https://godoc.org/io#Pipe)







