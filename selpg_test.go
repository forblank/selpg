package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"strings"
	"testing"
	"time"

	pflag "github.com/spf13/pflag"
)

func TestProcessArgs(t *testing.T) {

	cases := []struct {
		cmd          []string
		expectedArgs selpgArgs
	}{
		{
			[]string{"selpg", "-s1", "-e1"},
			selpgArgs{1, 1, "", 10, false, ""},
		},
		{
			[]string{"selpg", "-s1", "-e1", "input.txt"},
			selpgArgs{1, 1, "input.txt", 10, false, ""},
		},
		{
			[]string{"selpg", "-s2", "-e2", "-doutput.txt"},
			selpgArgs{2, 2, "", 10, false, "output.txt"},
		},
		{
			[]string{"selpg", "-s1", "-e2", "-l15"},
			selpgArgs{1, 2, "", 15, false, ""},
		},
		{
			[]string{"selpg", "-s1", "-e2", "-f"},
			selpgArgs{1, 2, "", 10, true, ""},
		},
		{
			[]string{"selpg", "-s1", "-e2", "-l15", "-doutput.txt", "input.txt"},
			selpgArgs{1, 2, "input.txt", 15, false, "output.txt"},
		},
	}

	for i, c := range cases {
		var args selpgArgs
		os.Args = c.cmd
		pflag.CommandLine = pflag.NewFlagSet(os.Args[0], pflag.ExitOnError)
		ProcessArgs(&args)
		if args != c.expectedArgs {
			t.Errorf("case %v: got %v, expected %v", i, args, c.expectedArgs)
		}
	}

}

func ExampleProcessArgs() {
	var args selpgArgs
	os.Args = []string{"selpg", "-s1", "-e2", "-l15", "-doutput.txt", "input.txt"}
	pflag.CommandLine = pflag.NewFlagSet(os.Args[0], pflag.ExitOnError)
	ProcessArgs(&args)
	fmt.Println(args)
	// Output: {1 2 input.txt 15 false output.txt}

}

func TestProcessInput(t *testing.T) {

	cases := []selpgArgs{
		selpgArgs{1, 1, "input.txt", 10, false, "output.txt"},
		selpgArgs{2, 2, "input.txt", 15, false, "output.txt"},
		selpgArgs{1, 2, "input.txt", 10, false, "output.txt"},
		selpgArgs{2, 3, "input.txt", 5, false, "output.txt"},
	}

	for i, args := range cases {
		ProcessInput(&args)
		time.Sleep(20 * time.Millisecond)
		pwd, _ := os.Getwd()
		outFile, _ := os.Open(pwd + "/" + args.printDest)
		lineCout := 0
		buffer := bufio.NewReader(outFile)

		for {
			line, err := buffer.ReadString('\n')
			if err == io.EOF {
				break
			}
			lineCout++
			line = strings.Replace(line, "\n", "", -1)
			expectedLine := "Line" + fmt.Sprint((args.startPage-1)*args.pageLen+lineCout)
			if line != expectedLine {
				t.Errorf("case %v: got %v, expected %v", i, line, expectedLine)
			}
		}
	}
}

func ExampleProcessInput() {
	args := selpgArgs{1, 1, "input.txt", 10, false, ""}
	ProcessInput(&args)
	//Output: Line1
	// Line2
	// Line3
	// Line4
	// Line5
	// Line6
	// Line7
	// Line8
	// Line9
	// Line10
}
